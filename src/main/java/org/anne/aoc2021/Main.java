package org.anne.aoc2021;

import org.anne.util.Utils;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main( String[] args ) {
        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                        .setUrls(ClasspathHelper.forPackage("org.anne.aoc2021"))
                        .setScanners(Scanners.SubTypes.filterResultsBy(c -> true)));
        List<Class<?>> allClasses = new java.util.ArrayList<>(List.copyOf(reflections.getSubTypesOf(Object.class)));

        allClasses.stream()
                .filter(clazz -> Utils.last(clazz.getCanonicalName().split("\\.")).matches(Utils.pattern))
                .sorted(Comparator.comparingInt(Utils::getDayNumber))
                .forEach(Main::executeClazz);
    }

    private static void executeClazz(Class<?> clazz) {
        try {
            final Method method = clazz.getMethod("main", String[].class);
            long start = System.nanoTime();
            method.invoke(null, (Object) null);
            long elapsed = System.nanoTime() - start;
            System.out.println("Executed in " + elapsed / 1000000 + "ms");
            System.out.println();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
