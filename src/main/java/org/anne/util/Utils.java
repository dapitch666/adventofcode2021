package org.anne.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static final String pattern = "(Day)([1-9]|1[0-9]|2[0-5])";

    public static List<String> readFile(String filename) {
        String filePath = "src/main/resources/aoc2021/" + filename;
        try {
            return Files.readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            System.err.format("There was an Error reading the File: %s%n", e);
            return new ArrayList<>();
        }
    }

    public static List<Integer> readFileInteger(String filename) {
        String filePath = "src/main/resources/aoc2021/" + filename;
        try {
            return Files.readAllLines(Paths.get(filePath)).stream().map(Integer::parseInt).collect(Collectors.toList());
        } catch (IOException | NumberFormatException e) {
            System.err.format("There was an Error reading the File: %s%n", e);
            return new ArrayList<>();
        }
    }

    public static String readFileOneLine(String filename) {
        String filePath = "src/main/resources/aoc2021/" + filename;
        try {
            return Files.readAllLines(Paths.get(filePath)).get(0);
        } catch (IOException | NumberFormatException e) {
            System.err.format("There was an Error reading the File: %s%n", e);
            return "";
        }
    }

    public static List<Integer> readFileIntegerOneLine(String filename) {
        String filePath = "src/main/resources/aoc2021/" + filename;
        try {
            return Arrays.stream(Files.readAllLines(Paths.get(filePath)).get(0).split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
        } catch (IOException | NumberFormatException e) {
            System.err.format("There was an Error reading the File: %s%n", e);
            return new ArrayList<>();
        }
    }

    public static <T> T last(T[] array) {
        return array[array.length - 1];
    }

    public static int getDayNumber(Class<?> clazz) {
        return Integer.parseInt(last(clazz.getCanonicalName().split("\\.")).replaceAll(pattern, "$2"));
    }

    public static HashMap<Character, Integer> characterCount(String inputString)
    {
        HashMap<Character, Integer> charCountMap = new HashMap<>();
        char[] strArray = inputString.toCharArray();
        for (char c : strArray) {
            if (charCountMap.containsKey(c)) {
                charCountMap.put(c, charCountMap.get(c) + 1);
            }
            else {
                charCountMap.put(c, 1);
            }
        }
        return charCountMap;
    }

    public static void print2dIntArray(int[][] array) {
        for (int[] ints : array) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < array[0].length; j++) {
                sb.append(ints[j] == 0 ? "." : "#");
            }
            System.out.println(sb);
        }
    }

    public static void print2dCharArray(char[][] array) {
        for (char[] chars : array) {
            System.out.println(new String(chars));
        }
    }
}
