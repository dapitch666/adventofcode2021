package org.anne.aoc2021;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day23Test {
    final List<String> input = Arrays.asList(
            "#############",
            "#...........#",
            "###B#C#B#D###",
            "  #A#D#C#A#  ",
            "  #########  "
    );

    @Test
    void part1() {
        assertEquals(12521, Day23.part1(input));
    }

    @Test
    void part2() {
        assertEquals(44169, Day23.part2(input));
    }
}