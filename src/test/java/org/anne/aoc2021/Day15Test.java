package org.anne.aoc2021;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day15Test {
    final List<String> input = Arrays.asList(
            "1163751742",
            "1381373672",
            "2136511328",
            "3694931569",
            "7463417111",
            "1319128137",
            "1359912421",
            "3125421639",
            "1293138521",
            "2311944581"
    );

    final List<String> smallInput = Arrays.asList(
            "12",
            "34"
    );

    final List<String> mediumInput = Arrays.asList(
            "1223",
            "3445",
            "2334",
            "4556"
    );

    @Test
    void part1() {
        assertEquals(6L, Day15.part1(smallInput));
        assertEquals(40L, Day15.part1(input));
        assertEquals(21L, Day15.part1(mediumInput));
    }

    @Test
    void part2() {
        assertEquals(73L, Day15.part2(smallInput));
        assertEquals(315L, Day15.part2(input));
    }
}