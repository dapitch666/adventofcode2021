package org.anne.aoc2021;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day9Test {
    final List<String> input = Arrays.asList("2199943210", "3987894921", "9856789892", "8767896789", "9899965678");

    @Test
    void part1() {
        assertEquals(15, Day9.part1(input));
    }

    @Test
    void part2() {
        assertEquals(1134L, Day9.part2(input));
    }
}