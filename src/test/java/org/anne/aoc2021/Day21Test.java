package org.anne.aoc2021;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day21Test {
    final List<String> input = Arrays.asList(
            "Player 1 starting position: 4",
            "Player 2 starting position: 8"
    );

    @Test
    void part1() {
        assertEquals(739785L, Day21.part1(input));
    }

    @Test
    void part2() {
        assertEquals(444356092776315L, Day21.part2(input));
    }
}